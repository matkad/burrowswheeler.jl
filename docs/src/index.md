# BurrowsWheeler

Set of functions operating on BioSequence type from BioJulia.

```@autodocs
Modules = [BurrowsWheeler]
Order   = [:function, :type]
```
