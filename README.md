# BurrowsWheeler.jl

[![pipeline status](https://gitlab.com/matkad/burrowswheeler.jl/badges/master/pipeline.svg)](https://gitlab.com/matkad/burrowswheeler.jl/commits/master)
[![coverage report](https://gitlab.com/matkad/burrowswheeler.jl/badges/master/coverage.svg)](https://gitlab.com/matkad/burrowswheeler.jl/commits/master)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://matkad.gitlab.io/burrowswheeler.jl/)

Julia package implementing basic Burrows-Wheeler transform algorithm.

